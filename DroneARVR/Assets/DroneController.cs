﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DroneController : MonoBehaviour {
    public GameObject droneTarget;
    public float droneSpeed;
    bool moving = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
       if (Cardboard.SDK.Triggered && !moving)
        {
            moving = true;
            Sequence droneSequence = DOTween.Sequence();
            float flightTime = Vector3.Distance(transform.position, droneTarget.transform.position)/droneSpeed;
            droneSequence.Append(transform.DORotate(new Vector3(0, transform.rotation.y, 0), .125f, RotateMode.Fast));
            //droneSequence.Append(transform.DOShakeRotation(flightTime, 5, 5, 45));
            droneSequence.Append(transform.DOShakeRotation(flightTime, new Vector3(5, 15, 5), 2, 45));
            droneSequence.Join(transform.DOMove(droneTarget.transform.position, flightTime,false).SetEase(Ease.InOutCubic));
            droneSequence.Join(transform.DOShakePosition(flightTime, .05f, 0, 45, false).SetEase(Ease.InOutCubic));
            droneSequence.Append(transform.DORotate(new Vector3(0, transform.rotation.y, 0), .125f, RotateMode.Fast));
            droneSequence.OnComplete(movementDone);
            //transform.DOLookAt(droneTarget.transform.position, flightTime/2, AxisConstraint.None, Vector3.up).SetEase(Ease.InOutCubic);
        }
        if (!moving)
        {
            //transform.DOShakeRotation(.25f, 1, 0, 45);
            transform.DOShakeRotation(.5f, new Vector3(.75f, 5f, .75f), 2, 45);
            transform.DOShakePosition(.5f, .010f, 0, 45, false);
        }

    }
    void movementDone()
    {
        moving = false;
    }
}
