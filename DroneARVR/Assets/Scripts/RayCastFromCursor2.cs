﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class RayCastFromCursor2 : MonoBehaviour
{
    //public GameObject cardboardMainObject;
    // Use this for initialization
    void Start()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }
    void Update()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        if (Cardboard.SDK.Triggered)
        {
            Ray ray;
            RaycastHit raycastHit;
            print("Mouse Down");
            if (Cardboard.SDK.VRModeEnabled)
            {
                Physics.Raycast(transform.position, transform.forward, out raycastHit);
            }
            else
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Physics.Raycast(ray, out raycastHit);
            }
            if (raycastHit.collider != null)
            {
                print("Hit something");
                if (raycastHit.collider.tag == "Spawner")
                {
                    print("Looking at Spawner");

                    print("Mouse Down");
                    //sprint("Spawn_Cursor");
                    //raycastHit.collider.transform.GetComponent<Create_Turtle>().SpawnTurtle();

                }
                if (raycastHit.collider.tag == "VrToggle")
                {
                    ARVRToggle();
                }
            }
        }
    }
    public void ARVRToggle()
    {
        //cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled = !(cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled);
        Cardboard.SDK.VRModeEnabled = !(Cardboard.SDK.VRModeEnabled);
    }
}
