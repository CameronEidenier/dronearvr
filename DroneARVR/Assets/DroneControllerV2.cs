﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DroneControllerV2 : MonoBehaviour {
    public GameObject droneTarget;
    public float droneThrust;
    public float turningSpeed;
    float distance;
    bool moving = false;
    Rigidbody rb;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();	    
	}
	
	// Update is called once per frame
	void Update () {
        distance = Vector3.Distance(transform.position, droneTarget.transform.position);
        if (distance > .6f){
            //transform.LookAt(droneTarget.transform);
            var targetRotation = Quaternion.LookRotation(droneTarget.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turningSpeed * Time.deltaTime);
            rb.AddForce(transform.forward * droneThrust*distance);
        }
        else {
            transform.DORotate(new Vector3(0, transform.rotation.y, 0), 1f, RotateMode.Fast);
        }
	}
}
